using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupText : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyText());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator DestroyText()
    {
        yield return new WaitForSeconds(4f);

        Destroy(gameObject);
    }
}
