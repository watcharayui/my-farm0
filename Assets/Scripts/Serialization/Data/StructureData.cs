using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StructureData
{
    public StructureType type;

    public int prefabID; //Structure ID to generate prefab when load

    public bool isFunctional;

    public int health;

    public Vector3 position;
    public Quaternion rotation;
}
