using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class JSONManager : MonoBehaviour
{
    public class myData
    {
        public int myInt;
        public Vector3 myPos;
    }

    myData save;

    // Start is called before the first frame update
    void Start()
    {
        save = new myData { myInt = 2, myPos = new Vector3(3f, 4f, 5f) };
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            //Save();
            SaveJSON();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            //Load();
            LoadJSON();
        }
    }

    private void Save()
    {
        File.WriteAllText(Application.dataPath + "/Savedgame/save.txt", "Hello");
        Debug.Log("Saved");
    }

    private void Load()
    {
        string content = File.ReadAllText(Application.dataPath + "/Savedgame/save.txt");
        Debug.Log(content);
    }

    private void SaveJSON()
    {
        string jsonString = JsonUtility.ToJson(save);
        File.WriteAllText(Application.dataPath + "/Savedgame/saveJSON.json", jsonString);

        Debug.Log("Saved JSON");
    }

    private void LoadJSON()
    {
        string jsonString = File.ReadAllText(Application.dataPath + "/Savedgame/saveJSON.json");
        myData myData = JsonUtility.FromJson<myData>(jsonString);

        Debug.Log(myData.myInt);
        Debug.Log(myData.myPos);
    }
}
