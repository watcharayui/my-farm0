using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text moneyText;
    public Text staffText;
    public Text wheatText;
    public Text melonText;
    public Text cornText;
    public Text milkText;
    public Text appleText;

    public Text dayText;

    public static UI instance;

    public GameObject laborMarketPanel;
    public GameObject farmPanel;
    public GameObject loadPanel;

    [SerializeField] private Text _farmNameText;
    public Text FarmNameText { get { return _farmNameText; } set { _farmNameText = value; } }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        UpdateResourceUI();
        UpdateTimeUI();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateResourceUI()
    {
        moneyText.text = Office.instance.money.ToString();
        staffText.text = Office.instance.staff.Count.ToString();
        wheatText.text = Office.instance.wheat.ToString();
        melonText.text = Office.instance.melon.ToString();
        cornText.text = Office.instance.corn.ToString();
        milkText.text = Office.instance.milk.ToString();
        appleText.text = Office.instance.apple.ToString();
    }

    public void UpdateTimeUI()
    {
        dayText.text = GameManager.instance.Day.ToString();
    }

    public void ToggleLaborPanel()
    {
        if (!laborMarketPanel.activeInHierarchy)
        {
            laborMarketPanel.SetActive(true);
        }
        else
        {
            laborMarketPanel.SetActive(false);
        }
    }

    public void ToggleFarmPanel()
    {
        if (!farmPanel.activeInHierarchy)
        {
            CloseUpAllPanel();
            farmPanel.SetActive(true);
        }
        else
        {
            farmPanel.SetActive(false);
        }
    }

    public void ToggleLoadPanel()
    {
        if (!loadPanel.activeInHierarchy)
        {
            CloseUpAllPanel();
            loadPanel.SetActive(true);
        }
        else
        {
            loadPanel.SetActive(false);
        }
    }

    private void CloseUpAllPanel()
    {
        farmPanel.SetActive(false);
        laborMarketPanel.SetActive(false);
    }

    public void PlayBellSFX()
    {
        if (AudioManager.instance != null)
        {
            AudioManager.instance.PlaySFX(0);
        }
    }
}
