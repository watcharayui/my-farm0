using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveManager : MonoBehaviour
{
    [SerializeField] private string _saveName;
    [SerializeField] private string[] _saveFilePaths;

    public void OnSave() //Save Game Button
    {
        Time.timeScale = 0f;
        Settings.SaveSelect(_saveName);
    }

    public void OnLoad() //Load Game Button
    {
        Debug.Log(_saveName);

        if (_saveName == "")
        {
            return;
        }

        if (FindSaveFiles(_saveName))
        {
            Settings.LoadSelect(_saveName);
        }
    }

    public void GetLoadFiles()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/saves/"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/saves/");
        }

        _saveFilePaths = Directory.GetFiles(Application.persistentDataPath + "/saves/");
    }

    private bool FindSaveFiles(string saveName)
    {
        GetLoadFiles();

        for (int i = 0; i < _saveFilePaths.Length; i++)
        {
            string filename = _saveFilePaths[i].Replace(Application.persistentDataPath + "/saves/", ""); //trim path off (only file name remains)

            if (filename == saveName + ".sav")
            {
                return true;
            }
        }

        return false;
    }

    public void SelectSlot(int i)
    {
        _saveName = "savedgame" + i.ToString();
    }

    public void CloseLoadPanel()
    {
        if (gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
    }
}
