using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaborMarket : MonoBehaviour
{
    public GameObject staffPrefab;
    public GameObject staffParent;

    public GameObject staffCardPrefab;
    public GameObject staffCardParent;

    public List<GameObject> LaborInMarket;
    public List<GameObject> LaborCardInMarket;

    [SerializeField] private int maxStaffInMarket = 20;

    private string[] maleName = { "Ben",  "John", "Sam", "Frank", "Joey", "Brandon", "Dan", "Peter", "Gary", "George"
                                 ,"Donald", "Richard", "Edward", "Brian", "Jason", "Anthony", "Cody", "Darren", "Adam", "Bruce"};

    private string[] femaleName = { "Ann", "Mary", "Jane", "Clara", "Tara", "Christina", "Angie", "Laurel", "Lara", "Penny"
                                    ,"Rosa", "Anya", "Ramona", "Katelyn", "Tiana", "Sunny", "Lillie", "Greta", "Kelly", "Hana"};

    public static LaborMarket instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        GenerateCandidate();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerateCandidate()
    {
        for (int i = 0; i < maxStaffInMarket; i++)
        {
            GameObject staffObj = Instantiate(staffPrefab, staffParent.transform);

            Staff s = staffObj.GetComponent<Staff>();

            s.ID = i;
            s.InitiateCharID(Random.Range(0, 5));
            s.SetGender();

            s.staffName = SetName(s);
            s.dailyWage = Random.Range(80, 120);

            LaborInMarket.Add(staffObj);

            GameObject cardObj = InitializeLaborCard(s);

            LaborCardInMarket.Add(cardObj);
        }
    }

    private string SetName(Staff staff)
    {
        int r = Random.Range(0, 20);

        string name;

        if (staff.StaffGender == Gender.male)
        {
            name = maleName[r];
        }
        else
        {
            name = femaleName[r];
        }

        return name;
    }

    private GameObject InitializeLaborCard(Staff s)
    {
        GameObject staffCard = Instantiate(staffCardPrefab, staffCardParent.transform);

        StaffCard card = staffCard.GetComponent<StaffCard>();
        card.UpdateID(s.ID);
        card.UpdateProfilePic(s.charFacePic[s.CharFaceID]);
        card.UpdateProfileName(s.staffName);
        card.UpdateWage(s.dailyWage);

        return staffCard;
    }
}
