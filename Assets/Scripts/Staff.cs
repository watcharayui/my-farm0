using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public enum UnitState
{
    Idle,
    Walking,
    Watering,
    Plowing,
    Sowing,
    Harvesting
}

public enum Gender
{
    male,
    female
}

public class Staff : MonoBehaviour
{
    private int _ID;
    public int ID { get { return _ID; } set { _ID = value; } }

    private int _charSkinID;
    public int CharSkinID { get { return _charSkinID; } set { _charSkinID = value; } }
    public GameObject[] charSkin;

    private int _charFaceID;
    public int CharFaceID { get { return _charFaceID; } set { _charFaceID = value; } }
    public Sprite[] charFacePic;

    public string staffName;
    public int dailyWage;

    [SerializeField] private Gender _staffGender = Gender.male;
    public Gender StaffGender { get { return _staffGender; } set { _staffGender = value; } }

    [SerializeField] private bool hired = false;
    public bool Hired { get { return hired; } set { hired = value; } }

    private NavMeshAgent navAgent;
    public NavMeshAgent NavAgent { get { return navAgent; } set { navAgent = value; } }

    private float distance; //distance to destination

    [SerializeField] private GameObject targetStructure;
    public GameObject TargetStructure { get { return targetStructure; } set { targetStructure = value; } }

    private float CheckStateTimer = 0f; //Timer
    private float CheckStateTimeWait = 0.5f;

    [SerializeField] private UnitState state;

    [SerializeField] private List<GameObject> tools;

    public UnityEvent<UnitState> onStateChange;

    void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //state = UnitState.Idle;
    }

    // Update is called once per frame
    void Update()
    {
        CheckStaffState();
    }

    public void SetState(UnitState st)
    {
        if (onStateChange != null)
        {
            onStateChange.Invoke(st);
        }

        state = st;
    }

    public UnitState CheckState()
    {
        return state;
    }

    public void SetTargetStructure(GameObject t)
    {
        targetStructure = t;
    }

    private void CheckStaffState()
    {
        CheckStateTimer += Time.deltaTime;

        if (CheckStateTimer >= CheckStateTimeWait)
        {
            CheckStateTimer = 0;
            SwitchStaffState();
        }
    }

    public void SwitchStaffState()
    {
        switch (state)
        {
            case UnitState.Walking:
                {
                    WalkUpdate();
                    break;
                }
        }
    }

    private void WalkUpdate()
    {
        distance = Vector3.Distance(navAgent.destination, transform.position);

        if (distance <= 3f)
        {
            navAgent.isStopped = true;
            SetState(UnitState.Idle);
        }
    }

    public void InitiateCharID(int i)
    {
        _charSkinID = i;
        _charFaceID = i;
    }

    public void SetGender()
    {
        if (_charSkinID == 1 || _charSkinID == 4)
        {
            _staffGender = Gender.female;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log("OnTrigger");

        if (other.gameObject != targetStructure)
        {
            return;
        }

        //Debug.Log("Found target");

        Farm farm = other.gameObject.GetComponent<Farm>();

        //Debug.Log(farm);

        if ((farm != null) && (farm.hp < 100)) // && (navAgent.isStopped == true)
        {
            //Debug.Log("Start Working");

            switch (farm.stage)
            {
                case FarmStage.plowing:
                    {
                        SetState(UnitState.Plowing);
                        EnableHoe();
                        farm.CheckTimeForWork();
                        break;
                    }
                case FarmStage.sowing:
                    {
                        SetState(UnitState.Sowing);
                        DisableAllTools();
                        farm.CheckTimeForWork();
                        break;
                    }
                case FarmStage.maintaining:
                    {
                        SetState(UnitState.Watering);
                        EnableCan();
                        farm.CheckTimeForWork();
                        break;
                    }
                case FarmStage.harvesting:
                    {
                        SetState(UnitState.Harvesting);
                        DisableAllTools();
                        farm.CheckTimeForWork();
                        break;
                    }
            }
        }
    }

    public void ChangeCharSkin()
    {
        for (int i = 0; i < charSkin.Length; i++)
        {
            if (i == _charSkinID)
            {
                charSkin[i].SetActive(true);
            }
            else
            {
                charSkin[i].SetActive(false);
            }
        }
    }

    public void HideCharSkin()
    {
        foreach (GameObject obj in charSkin)
        {
            obj.SetActive(false);
        }
    }

    public void SetToWalk(Vector3 dest)
    {
        SetState(UnitState.Walking);

        navAgent.SetDestination(dest);
        navAgent.isStopped = false;
    }

    public void DisableAllTools()
    {
        foreach (GameObject t in tools)
        {
            t.SetActive(false);
        }
    }

    public void EnableHoe()
    {
        DisableAllTools();
        tools[0].SetActive(true); //Hoe
    }

    public void EnableCan()
    {
        DisableAllTools();
        tools[1].SetActive(true); //Can
    }
}
