using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StructureManager : MonoBehaviour
{
    [SerializeField] private bool _isConstructing;
    [SerializeField] private bool _isDemolishing;

    public GameObject curBuildingPrefab;
    public GameObject buildingParent;

    [SerializeField] private float _cursorUpdateRate = 0.05f;
    [SerializeField] private float _Timer;
    [SerializeField] private Vector3 _curCursorPos;

    public GameObject buildingCursor;
    public GameObject demolishCursor;
    public GameObject gridPlane;

    private GameObject ghostBuilding;

    [SerializeField] private GameObject _curStructure; //Currently selected structure
    public GameObject CurStructure { get { return _curStructure; } set { _curStructure = value; } }

    [SerializeField] private GameObject[] structurePrefab;

    private Camera _cam;

    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
        {
            CancelStructurePlacement();
        }

        _Timer += Time.deltaTime;

        _curCursorPos = Formula.instance.GetCurTilePosition();

        if (_Timer >= _cursorUpdateRate)
        {
            _Timer = 0;

            if (_isConstructing) //Mode Construct
            {
                buildingCursor.transform.position = _curCursorPos;
                gridPlane.SetActive(true);
            }
            else if (_isDemolishing) //Mode Demolish
            {
                demolishCursor.transform.position = _curCursorPos;
                gridPlane.SetActive(true);
            }
            else //Mode Play
            {
                gridPlane.SetActive(false);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (_isConstructing)
            {
                PlaceBuilding(); //Real Construction
            }
            else if (_isDemolishing)
            {
                Demolish();
            }
            else //Normal Gameplay
            {
                CheckLeftClick(Input.mousePosition);
            }
        }
    }

    public void BeginNewBuildingPlacement(GameObject prefab)
    {
        //check money
        if (CheckMoney(prefab) == false)
        {
            return;
        }

        _isDemolishing = false;
        _isConstructing = true;

        curBuildingPrefab = prefab;

        //Instantiate Ghost Building
        ghostBuilding = Instantiate(curBuildingPrefab, _curCursorPos, Quaternion.identity);
        ghostBuilding.GetComponent<FindBuildingSite>().plane.SetActive(true);

        buildingCursor = ghostBuilding;

        buildingCursor.SetActive(true);
    }

    private bool CheckMoney(GameObject obj)
    {
        int cost = obj.GetComponent<Structure>().costToBuild;

        if (cost <= Office.instance.money)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void CancelStructurePlacement()
    {
        _isConstructing = false;

        if (buildingCursor != null)
        {
            buildingCursor.SetActive(false);
        }

        if (ghostBuilding != null)
        {
            Destroy(ghostBuilding);
        }
    }

    private void PlaceBuilding()
    {
        if (buildingCursor.GetComponent<FindBuildingSite>().CanBuild == false)
        {
            return;
        }

        GameObject structureObj = Instantiate(curBuildingPrefab,
                                                _curCursorPos,
                                                Quaternion.identity,
                                                buildingParent.transform);

        Structure s = structureObj.GetComponent<Structure>();

        Office.instance.AddBuilding(s);
        DeductMoney(s.costToBuild);
    }

    public void PlaceBuilding(StructureData sd)
    {
        GameObject structObj = Instantiate(structurePrefab[sd.prefabID], sd.position, sd.rotation, buildingParent.transform);
        Structure s = structObj.GetComponent<Structure>();

        s.functional = sd.isFunctional;
        s.hp = sd.health;

        Office.instance.structures.Add(s);
    }

    private void DeductMoney(int cost)
    {
        Office.instance.money -= cost;
        UI.instance.UpdateResourceUI();
    }

    public void ToggleDemolish()
    {
        _isConstructing = false;
        _isDemolishing = !_isDemolishing;

        gridPlane.SetActive(_isDemolishing);
        demolishCursor.SetActive(_isDemolishing);
    }

    public void Demolish()
    {
        Structure s = Office.instance.structures.Find(x => x.transform.position == _curCursorPos);

        if (s != null)
        {
            Office.instance.RemoveBuilding(s);
        }

        UI.instance.UpdateResourceUI();
    }

    public void CallStaff()
    {
        Office.instance.SendStaff(CurStructure);
        UI.instance.UpdateResourceUI();
    }

    public void CheckLeftClick(Vector2 mousePos)
    {
        Ray ray = _cam.ScreenPointToRay(mousePos);
        RaycastHit hit;

        //if we left click something
        if (Physics.Raycast(ray, out hit, 1000))
        {
            //Mouse over UI
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            CurStructure = hit.collider.gameObject;

            switch (hit.collider.tag)
            {
                case "Farm": // if we click Object with Farm tag 
                    {
                        //Structure s = CurStructure.GetComponent<Structure>();
                        OpenFarmPanel();

                        break;
                    }
            }
        }
    }

    public void OpenFarmPanel()
    {
        string name = CurStructure.GetComponent<Farm>().structureName;

        UI.instance.FarmNameText.text = name;
        UI.instance.ToggleFarmPanel();
    }
}
