using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Market : MonoBehaviour
{
    [SerializeField] private int _curPriceWheat;
    public int CurPriceWheat { get { return _curPriceWheat; } set { _curPriceWheat = value; } }

    [SerializeField] private int _curPriceMelon;
    public int CurPriceMelon { get { return _curPriceMelon; } set { _curPriceMelon = value; } }

    [SerializeField] private int _curPriceCorn;
    public int CurPriceCorn { get { return _curPriceCorn; } set { _curPriceCorn = value; } }

    [SerializeField] private int _curPriceMilk;
    public int CurPriceMilk { get { return _curPriceMilk; } set { _curPriceMilk = value; } }

    [SerializeField] private int _curPriceApple;
    public int CurPriceApple { get { return _curPriceApple; } set { _curPriceApple = value; } }

    public UnityEvent ResourceUpdate;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SellWheat()
    {
        Office.instance.money += Office.instance.wheat * _curPriceWheat;
        Office.instance.wheat = 0;

        //UI.instance.UpdateResourceUI();
        ResourceUpdate.Invoke();
    }

    public void SellMelon()
    {
        Office.instance.money += Office.instance.melon * _curPriceMelon;
        Office.instance.melon = 0;
        
        //UI.instance.UpdateResourceUI();
        ResourceUpdate.Invoke();
    }

    public void SellCorn()
    {
        Office.instance.money += Office.instance.corn * _curPriceCorn;
        Office.instance.corn = 0;
        
        //UI.instance.UpdateResourceUI();
        ResourceUpdate.Invoke();
    }

    public void SellMilk()
    {
        Office.instance.money += Office.instance.milk * _curPriceMilk;
        Office.instance.milk = 0;
        
        //UI.instance.UpdateResourceUI();
        ResourceUpdate.Invoke();
    }

    public void SellApple()
    {
        Office.instance.money += Office.instance.apple * _curPriceApple;
        Office.instance.apple = 0;
        
        //UI.instance.UpdateResourceUI();
        ResourceUpdate.Invoke();
    }

    public void SellAll()
    {
        SellWheat();
        SellMelon();
        SellCorn();
        SellMilk();
        SellApple();
    }
}
