using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private string newScene;

    [SerializeField] private GameObject LoadPanel;

    // Start is called before the first frame update
    void Start()
    {
        if (AudioManager.instance != null)
        {
            //AudioManager.instance.PlayBGM(0);
            AudioManager.instance.PlayRandomBGM();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (AudioManager.instance != null)
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                AudioManager.instance.PlayRandomBGM();
            }
        }
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene(newScene);
    }

    public void ShowLoadPanel()
    {
        LoadPanel.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
