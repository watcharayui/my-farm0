using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindBuildingSite : MonoBehaviour
{
    [SerializeField] private bool _canBuild = false;
    public bool CanBuild { get { return _canBuild; } set { _canBuild = value; } }

    public GameObject plane;

    private Renderer _pRenderer;

    void Awake()
    {
        _pRenderer = plane.GetComponent<Renderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _pRenderer.material.color = Color.green;

        _canBuild = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Building" || other.tag == "House" || other.tag == "Farm" || other.tag == "Road")
        {
            _pRenderer.material.color = Color.red;
            _canBuild = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Building" || other.tag == "House" || other.tag == "Farm" || other.tag == "Road")
        {
            _pRenderer.material.color = Color.red;
            _canBuild = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Building" || other.tag == "House" || other.tag == "Farm" || other.tag == "Road")
        {
            _pRenderer.material.color = Color.green;
            _canBuild = true;
        }
    }
}
