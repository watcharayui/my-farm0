using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int money;
    public int wheat;
    public int melon;
    public int corn;
    public int milk;
    public int apple;

    public int dailyCostWages;

    public List<StructureData> structureData = new List<StructureData>();
    public List<StaffData> staffData = new List<StaffData>();

    public static GameData instance = new GameData();

    //Save GameManager into Savedata
    public void Save()
    {
        AddStructures();
        AddPlayerResource();
    }

    public void ClearData()
    {
        instance = new GameData();
    }

    public void ShowData()
    {
        foreach (StaffData s in staffData)
        {
            Debug.Log(s + "\n");
        }
    }

    void AddStructures()
    {
        for (int i = 0; i < Office.instance.structures.Count; i++)
        {
            Structure s = Office.instance.structures[i];

            if (s != null)
            {
                structureData.Add(s.GetSaveData());
            }
        }
    }

    void AddPlayerResource()
    {
        money = Office.instance.money;
        wheat = Office.instance.wheat;
        melon = Office.instance.melon;
        corn = Office.instance.corn;
        milk = Office.instance.milk;
        apple = Office.instance.apple;
    }
}
