using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum StructureType
{
    road,
    building,
    wheat,
    melon,
    corn,
    milk,
    apple
}

public abstract class Structure : MonoBehaviour
{
    public StructureType structureType;

    public bool functional = false; //for farm, it can auto-grow or not

    public string structureName;

    public int hp = 1;
    public int costToBuild;

    public int ID;

    [SerializeField] private GameObject _popUpPrefab;

    StructureData saveData;

    public virtual StructureData GetSaveData()
    {
        saveData = new StructureData();

        Debug.Log(ID);
        saveData.prefabID = ID;

        saveData.isFunctional = functional;
        saveData.health = hp;

        saveData.position = transform.position;
        saveData.rotation = transform.rotation;

        return saveData;
    }

    protected void PopUpText(int num)
    {
        GameObject popUpObj =  Instantiate(_popUpPrefab, new Vector3(transform.position.x, 10f, transform.position.z), Quaternion.identity);
        TextMeshPro tmp = popUpObj.GetComponent<TextMeshPro>();
        tmp.SetText("+$" + num.ToString());
    }
}
