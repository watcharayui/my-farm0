using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private int day = 0;
    public int Day { get { return day; } set { day = value; } }

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (Settings.loadingGame == true)
        {
            SpawnStructure();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void SpawnStructure()
    {
        List<StructureData> tempStructureData = new List<StructureData>(GameData.instance.structureData);
        GameData.instance.ClearData();

        foreach(StructureData sd in tempStructureData)
        {
            GetComponent<StructureManager>().PlaceBuilding(sd);
        }
    }
}
