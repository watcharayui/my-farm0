using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource[] BGM;
    [SerializeField] private AudioSource[] SFX;

    public static AudioManager instance;

    [SerializeField] private int currentBGM;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckCurrentBGM() == false)
        {
            PlayRandomBGM();
        }
    }

    public void StopBGM()
    {
        for (int i = 0; i < BGM.Length; i++)
        {
            BGM[i].Stop();
        }
    }

    public void PlayBGM(int i)
    {
        if (BGM[i] != null)
        {
            currentBGM = i;

            StopBGM();
            BGM[i].Play();
        }
    }

    public void PlayRandomBGM()
    {
        int i = Random.Range(0, BGM.Length);

        PlayBGM(i);
    }

    public bool CheckCurrentBGM()
    {
        if (BGM[currentBGM].isPlaying)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PlaySFX(int i)
    {
        if (SFX[i] != null)
        {
            SFX[i].Play();
        }
    }
}
