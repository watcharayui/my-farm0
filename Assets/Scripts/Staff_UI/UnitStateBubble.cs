using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitStateBubble : MonoBehaviour
{
    [SerializeField] private Image stateImg;

    [SerializeField] private Sprite Idle;
    [SerializeField] private Sprite Walking;
    [SerializeField] private Sprite Watering;
    [SerializeField] private Sprite Plowing;
    [SerializeField] private Sprite Sowing;
    [SerializeField] private Sprite Harvesting;

    public void OnStateChange(UnitState state)
    {
        switch (state)
        {
            case UnitState.Idle:
                {
                    stateImg.sprite = Idle;
                    break;
                }
            case UnitState.Walking:
                {
                    stateImg.sprite = Walking;
                    break;
                }
            case UnitState.Watering:
                {
                    stateImg.sprite = Watering;
                    break;
                }
            case UnitState.Plowing:
                {
                    stateImg.sprite = Plowing;
                    break;
                }
            case UnitState.Sowing:
                {
                    stateImg.sprite = Sowing;
                    break;
                }
            case UnitState.Harvesting:
                {
                    stateImg.sprite = Harvesting;
                    break;
                }
        }     
    }
}
