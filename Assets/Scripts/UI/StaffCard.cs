using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaffCard : MonoBehaviour
{
    [SerializeField] private int _ID;
    [SerializeField] private Image _profilePic;
    [SerializeField] private Text _candidateName;
    [SerializeField] private Text _IDText;
    [SerializeField] private Text _wage;

    [SerializeField] private Button HireButton;
    [SerializeField] private Button FireButton;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateID(int id)
    {
        _ID = id;
        UpdateIDText(_ID.ToString());
    }

    private void UpdateIDText(string s)
    {
        _IDText.text = "ID: " + s;
    }

    public void UpdateProfilePic(Sprite s)
    {
        _profilePic.sprite = s;
    }

    public void UpdateWage(int n)
    {
        _wage.text = n.ToString();
    }

    public void UpdateProfileName(string s)
    {
        _candidateName.text = s;
    }

    public void Hire()
    {
        bool hired = Office.instance.ToHireStaff(LaborMarket.instance.LaborInMarket[_ID]);

        if (hired)
        {
            gameObject.SetActive(false);
        }
    }
}
