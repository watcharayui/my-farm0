using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Settings
{
    public static bool loadingGame = false;

    public static void LoadSelect(string saveName) //Load game from a slot
    {
        loadingGame = true; //it is loading a saved game

        GameData.instance.ClearData(); //Clear data in GameData
        GameData.instance = (GameData)Serialization.Load(Application.persistentDataPath + "/saves/" + saveName + ".sav");

        SceneManager.LoadScene("Scene01");

        Time.timeScale = 1f;
    }

    public static void SaveSelect(string saveName) //Save game to a slot
    {
        GameData.instance.Save(); //Game data into GameData
        Serialization.Save(saveName, GameData.instance); //Serialize GameData into a file
    }
}
