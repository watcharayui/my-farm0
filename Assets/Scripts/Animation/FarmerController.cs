using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmerController : MonoBehaviour
{
    private Animator _anim;
    private Staff _staff;

    void Awake()
    {
        _anim = GetComponent<Animator>();
        _staff = GetComponent<Staff>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_staff.CheckState() == UnitState.Idle)
        {
            DisableAll();
            _anim.SetBool("isIdle", true);
        }

        if (_staff.CheckState() == UnitState.Walking)
        {
            DisableAll();
            _anim.SetBool("isWalk", true);
        }

        if (_staff.CheckState() == UnitState.Watering)
        {
            DisableAll();
            _anim.SetBool("isWater", true);
        }

        if (_staff.CheckState() == UnitState.Plowing)
        {
            DisableAll();
            _anim.SetBool("isPlow", true);
        }

        if (_staff.CheckState() == UnitState.Sowing)
        {
            DisableAll();
            _anim.SetBool("isSow", true);
        }

        if (_staff.CheckState() == UnitState.Harvesting)
        {
            DisableAll();
            _anim.SetBool("isHarvest", true);
        }
    }

    private void DisableAll()
    {
        _anim.SetBool("isIdle", false);
        _anim.SetBool("isWalk", false);
        _anim.SetBool("isWater", false);
        _anim.SetBool("isPlow", false);
        _anim.SetBool("isSow", false);
        _anim.SetBool("isHarvest", false);
    }
}
