using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FarmStage
{
    plowing,
    sowing,
    maintaining,
    harvesting
}

public class Farm : Structure
{
    public FarmStage stage = FarmStage.plowing;

    [SerializeField] private int _maxStaffNum = 3;
    public int MaxStaffNum { get { return _maxStaffNum; } set { _maxStaffNum = value; } }

    public int dayRequired; //Day until harvest
    public int dayPassed; //Day passed since last harvest
    public float produceTimer = 0f;
    private int secondsPerday = 10;

    private float WorkTimer = 0f; //Timer for working
    private float WorkTimeWait = 1f;

    public GameObject FarmUI;

    [SerializeField] private GameObject _crops;

    [SerializeField] private List<Staff> _workingStaff;
    public List<Staff> WorkingStaff { get { return _workingStaff; } set { _workingStaff = value; } }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckPlowing();
        CheckSowing();
        CheckMaintaining();
        CheckHarvesting();
    }

    private void Working()
    {
        hp += 3;
    }

    public void CheckTimeForWork()
    {
        WorkTimer += Time.deltaTime;

        if (WorkTimer >= WorkTimeWait)
        {
            WorkTimer = 0;
            Working();
        }
    }

    public void CheckPlowing()
    {
        if ((hp >= 100) && (stage == FarmStage.plowing))
        {
            stage = FarmStage.sowing;
            hp = 1;
        }
    }

    public void CheckSowing()
    {
        if ((hp >= 100) && (stage == FarmStage.sowing))
        {
            functional = true; //Plant will auto grow

            stage = FarmStage.maintaining;
            hp = 1;
        }
    }

    public void CheckMaintaining()
    {
        if ((hp >= 100) && (stage == FarmStage.maintaining))
        {
            produceTimer += Time.deltaTime;

            dayPassed = Mathf.CeilToInt(produceTimer / secondsPerday);

            if ((functional == true) && (dayPassed >= dayRequired))
            {
                produceTimer = 0;

                stage = FarmStage.harvesting;
                hp = 1;
            }
        }
    }

    public void CheckHarvesting()
    {
        if ((hp >= 100) && (stage == FarmStage.harvesting))
        {
            //harvest
            HarvestResult();

            hp = 1;
            stage = FarmStage.sowing;

            StartCoroutine(ShowCrops());
        }
    }

    private IEnumerator ShowCrops()
    {
        _crops.SetActive(false);

        yield return new WaitForSeconds(5f);

        _crops.SetActive(true);
    }

    public void HarvestResult()
    {
        switch (structureType)
        {
            case StructureType.wheat:
                {
                    Office.instance.wheat += 1000;
                    break;
                }
            case StructureType.melon:
                {
                    Office.instance.melon += 1000;
                    break;
                }
        }

        PopUpText(1000);
        UI.instance.UpdateResourceUI();
    }

    public void AddStaffToFarm(Staff staff)
    {
        _workingStaff.Add(staff);
    }
}
