using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float moveSpeed;

    [SerializeField] private float minZoomDist;
    [SerializeField] private float maxZoomDist;

    [SerializeField] private float zoomSpeed;
    [SerializeField] private float zoomModifier;

    private Camera _cam;

    [SerializeField] private Transform corner1, corner2;

    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Zoom();
        MoveByKB();
    }

    private void Zoom()
    {
        zoomModifier = Input.GetAxis("Mouse ScrollWheel");

        if (Input.GetKey(KeyCode.Z))
        {
            zoomModifier = 0.01f;
        }

        if (Input.GetKey(KeyCode.X))
        {
            zoomModifier = -0.01f;
        }

        float dist = Vector3.Distance(transform.position, _cam.transform.position);

        //Debug.Log(zoomModifier);

        if (dist < minZoomDist && zoomModifier > 0.0f)
        {
            return;
        }

        if (dist > maxZoomDist && zoomModifier < 0.0f)
        {
            return;
        }

        _cam.transform.position += _cam.transform.forward * zoomModifier * zoomSpeed;
    }

    private void MoveByKB()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");

        Vector3 dir = transform.forward * zInput + transform.right * xInput;

        transform.position += dir * moveSpeed * Time.deltaTime;
        transform.position = Clamp(corner1.position, corner2.position);
    }

    private Vector3 Clamp(Vector3 lowerLeft, Vector3 upperRight)
    {
        Vector3 pos = new Vector3(Mathf.Clamp(transform.position.x, lowerLeft.x, upperRight.x),
                                transform.position.y,
                                Mathf.Clamp(transform.position.z, lowerLeft.z, upperRight.z));

        return pos;
    }
}
