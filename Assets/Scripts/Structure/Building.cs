using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Structure
{
    [SerializeField] private int _spaceForStaff;
    public int SpaceForStaff { get { return _spaceForStaff; } set { _spaceForStaff = value; } }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}